package com.example.myhead;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    //lakukan deklarasi variabel
    ImageView hair, eyebrow, moustache, beard;
    CheckBox cekHair, cekEyebrow, cekMoustache, cekBeard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hair = findViewById(R.id.hair);
        eyebrow = findViewById(R.id.eyebrow);
        moustache = findViewById(R.id.moustache);
        beard = findViewById(R.id.beard);
        cekHair = findViewById(R.id.cekHair);
        cekEyebrow = findViewById(R.id.cekEyebrow);
        cekMoustache = findViewById(R.id.cekMoustache);
        cekBeard = findViewById(R.id.cekBeard);

        //Method untuk cek rambut
        cekHair.setOnClickListener(new View.OnClickListener() {
            //menentukan visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                hair.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });
        //Method untuk cek alis
        cekEyebrow.setOnClickListener(new View.OnClickListener() {
            //menentukan visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                eyebrow.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });
        //Method untuk cek kumis
        cekMoustache.setOnClickListener(new View.OnClickListener() {
            //menentukan visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                moustache.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });
        //Method untuk cek janggut
        cekBeard.setOnClickListener(new View.OnClickListener() {
            //menentukan visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                beard.setVisibility(visible ? View.VISIBLE : View.GONE);
            }
        });

    }
}